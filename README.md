# React App

Build the app `npm run build`

Run the app `npm start`

Run test `npm test`

Generate image `docker build -t react-skeleton .`

Run container `docker run -it -p 3001:3000 react-skeleton`